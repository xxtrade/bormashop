<?php
/**
 * Search Bar
 *
 * @author  Transvelo
 * @package Electro/Templates
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if( is_rtl() ) {
	$dir_value = 'rtl';
} else {
	$dir_value = 'ltr';
}

$navbar_search_text 		 = apply_filters( 'electro_navbar_search_placeholder', esc_html__( 'Search for products', 'electro' ) );
$navbar_search_dropdown_text = apply_filters( 'electro_navbar_search_dropdown_text', esc_html__( 'All Categories', 'electro' ) );


 if( is_woocommerce_activated() ) {
 	if ( function_exists( 'aws_get_search_form' ) ) { 
 		echo '<div class="site-search">'.aws_get_search_form().'</div>';

 	}
 }

