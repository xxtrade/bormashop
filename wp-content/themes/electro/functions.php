<?php
/**
 * electro engine room
 *
 * @package electro
 */
function is_lighthouse() {
     $user_agent = $_SERVER['HTTP_USER_AGENT']; 
    if(strpos( $_SERVER['HTTP_USER_AGENT'], 'Lighthouse') !== false) {
        return true;
    }  
}
/**
 * Initialize all the things.
 */
require get_template_directory() . '/inc/init.php';

/**
 * Note: Do not add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * http://codex.wordpress.org/Child_Themes
 */