<?php
function variable_price_format_filter( $price, $product ) {
    $prefix = 'от ';

    $min_price_regular = $product->get_variation_regular_price( 'min', true );
    $min_price_sale    = $product->get_variation_sale_price( 'min', true );
    $max_price = $product->get_variation_price( 'max', true );
    $min_price = $product->get_variation_price( 'min', true );

    $price = ( $min_price_sale == $min_price_regular ) ?
        wc_price( $min_price_regular ) :
        '<del>' . wc_price( $min_price_regular ) . '</del>' . '<ins>' . wc_price( $min_price_sale ) . '</ins>';

    return ( $min_price == $max_price ) ?
        $price :
        $prefix . $price;
}

add_filter( 'woocommerce_variable_sale_price_html', 'variable_price_format_filter', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'variable_price_format_filter', 10, 2 );


add_action( 'after_setup_theme', 'ec_child_remove_product_gallery_zoom_support', 20 );

function ec_child_remove_product_gallery_zoom_support() {
	remove_theme_support( 'wc-product-gallery-zoom' );
}


function electro_product_search() {
  if ( is_woocommerce_activated() ) { ?>
    <div class="site-search">
      <?php
        echo do_shortcode("[aws_search_form]");
      ?>
    </div>
  <?php
  }
}

add_filter( 'woocommerce_register_shop_order_post_statuses', 'bbloomer_register_custom_order_status' );
 
function bbloomer_register_custom_order_status( $order_statuses ){
     
    // Status must start with "wc-"
    $order_statuses['wc-new-status'] = array(                                            
    'label'                     => _x( 'Новый', 'Order status', 'woocommerce' ),
    'public'                    => false,                                            
    'exclude_from_search'       => false,                                            
    'show_in_admin_all_list'    => true,                                         
    'show_in_admin_status_list' => true,                                         
    'label_count'               => _n_noop( 'Новый <span class="count">(%s)</span>', 'Новый <span class="count">(%s)</span>', 'woocommerce' ),                                       
    );      
    return $order_statuses;
}
 
// ---------------------
// 2. Show Order Status in the Dropdown @ Single Order
 
add_filter( 'wc_order_statuses', 'bbloomer_show_custom_order_status' );
 
function bbloomer_show_custom_order_status( $order_statuses ) {    
    $order_statuses['wc-new-status'] = _x( 'Новый', 'Order status', 'woocommerce' );       
    return $order_statuses;
}
 
// ---------------------
// 3. Set Custom Order Status @ WooCommerce Checkout Process
 
add_action( 'woocommerce_thankyou', 'bbloomer_thankyou_change_order_status' );
 
function bbloomer_thankyou_change_order_status( $order_id ){
    if( ! $order_id ) return;
    $order = wc_get_order( $order_id );
 
    // Status without the "wc-" prefix
    $order->update_status( 'new-status' );
}

/**
 * Update CSS within in Admin
 */

function admin_style() {
  wp_enqueue_style('admin-styles', get_stylesheet_directory_uri() . '/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');

// URostov Make "in process orders" editable
add_filter( 'wc_order_is_editable', 'lets_make_processing_orders_editable', 10, 2 );
function lets_make_processing_orders_editable( $is_editable, $order ) {
    if ( $order->get_status() == 'processing' ) {
        $is_editable = true;
    }
 
    return $is_editable;
}

function display_order_weight_in_admin( $order ){  

global $post, $woocommerce, $the_order;
	
	if ( empty( $the_order ) || $the_order->id != $post->ID ) 
		$the_order = new WC_Order( $post->ID );
		$weight = 0;

		if ( sizeof( $the_order->get_items() ) > 0 ) {
			foreach( $the_order->get_items() as $item ) {
				if ( $item['product_id'] > 0 ) {
					$_product = $the_order->get_product_from_item( $item );
					if ( ! $_product->is_virtual() ) {
						$weight += $_product->get_weight() * $item['qty'];
					}
				}
			}
		}
		
	echo '<div style="padding-right: 350px">';
	
		if ( $weight > 0 )
			echo '<h4>Общий вес: <strong>' . $weight . ' ' . esc_attr( get_option('woocommerce_weight_unit' ) ) . '</strong></h4>';

		else 'N/A';
	
	echo '</div>';
}
add_action( 'woocommerce_admin_order_totals_after_shipping', 'display_order_weight_in_admin' );

/**
 * Unhook and remove WooCommerce default emails.
 */
add_action( 'woocommerce_email', 'unhook_those_pesky_emails' );

function unhook_those_pesky_emails( $email_class ) {

        /**
         * Hooks for sending emails during store events
         **/
        //remove_action( 'woocommerce_low_stock_notification', array( $email_class, 'low_stock' ) );
        //remove_action( 'woocommerce_no_stock_notification', array( $email_class, 'no_stock' ) );
        //remove_action( 'woocommerce_product_on_backorder_notification', array( $email_class, 'backorder' ) );
        
        // New order emails
        //remove_action( 'woocommerce_order_status_pending_to_processing_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
        
        remove_action( 'woocommerce_order_status_pending_to_completed_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
        //remove_action( 'woocommerce_order_status_pending_to_on-hold_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
        //remove_action( 'woocommerce_order_status_failed_to_processing_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
        remove_action( 'woocommerce_order_status_failed_to_completed_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
        //remove_action( 'woocommerce_order_status_failed_to_on-hold_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
        
        // Processing order emails
        //remove_action( 'woocommerce_order_status_pending_to_processing_notification', array( $email_class->emails['WC_Email_Customer_Processing_Order'], 'trigger' ) );
        //remove_action( 'woocommerce_order_status_pending_to_on-hold_notification', array( $email_class->emails['WC_Email_Customer_Processing_Order'], 'trigger' ) );
        
        // Completed order emails
        //remove_action( 'woocommerce_order_status_completed_notification', array( $email_class->emails['WC_Email_Customer_Completed_Order'], 'trigger' ) );
            
        // Note emails
        //remove_action( 'woocommerce_new_customer_note_notification', array( $email_class->emails['WC_Email_Customer_Note'], 'trigger' ) );
}

/*
 *  Add first letter uppercased in name in order form
 */
add_action( 'wp_enqueue_scripts', 'bs_order_form_method' );
function bs_order_form_method() {
    wp_register_script( 'custom-script', get_stylesheet_directory_uri() . '/assets/js/bs_order_form.js', array( 'jquery' ) ); 
    wp_enqueue_script( 'custom-script' ); 
}

/** * Move css and js in to footer */ 
function footer_enqueue_scripts(){ 
	remove_action('wp_head','wp_print_scripts'); 
	remove_action('wp_head','wp_print_head_scripts',9); 
	remove_action('wp_head','wp_enqueue_scripts',1); 
	add_action('wp_footer','wp_print_scripts',5); 
	add_action('wp_footer','wp_enqueue_scripts',5); 
	add_action('wp_footer','wp_print_head_scripts',5); 
} 
//add_action('after_setup_theme','footer_enqueue_scripts');

/*
 *  Remove script version
 */

function _remove_script_version( $src ){ 
    $parts = explode( '?', $src ); 	
    return $parts[0]; 
} 
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 ); 
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );


/*
 *  Remove CSS
 */

function remove_css() {

    wp_dequeue_style('wp-block-library');
    wp_dequeue_style('jquery-selectBox');
    wp_dequeue_style('yith-wcwl-font-awesome');
    wp_dequeue_style('yith-wcwl-main');
    wp_dequeue_style('contact-form-7');
    wp_dequeue_style('wcqu_quantity_styles');
    wp_dequeue_style('rs-plugin-settings');
    wp_dequeue_style('yith-wcwl-main');
    wp_dequeue_style('aws-style');
    wp_dequeue_style('electro-fonts');
    wp_dequeue_style('bootstrap');
    wp_dequeue_style('fontawesome'); 
    wp_dequeue_style('animate'); 
    wp_dequeue_style('font-electro'); 
    wp_dequeue_style('electro-style'); 
    wp_dequeue_style('electro-style-v2'); 
    wp_dequeue_style('electro-child-style'); 
    wp_dequeue_style('ywpar_frontend'); 
    wp_dequeue_style('ivpa-style'); 
    wp_dequeue_style('popup-maker-site'); 
    wp_dequeue_style('electro-custom-color'); 
    wp_dequeue_style('wc-featured-product-editor');
    wp_dequeue_style('wc-vendors');
    wp_dequeue_style('js_composer_front');
    wp_dequeue_style('prettyphoto');
    wp_dequeue_style('tc-epo-css');
    wp_dequeue_style('photoswipe');
    wp_dequeue_style('photoswipe-default-skin');


    wp_deregister_style('wp-block-library');
    wp_deregister_style('jquery-selectBox');
    wp_deregister_style('yith-wcwl-font-awesome');
    wp_deregister_style('yith-wcwl-main');
    wp_deregister_style('contact-form-7');
    wp_deregister_style('wcqu_quantity_styles');
    wp_deregister_style('rs-plugin-settings');
    wp_deregister_style('yith-wcwl-main');
    wp_deregister_style('aws-style');
    wp_deregister_style('electro-fonts');
    wp_deregister_style('bootstrap');
    wp_deregister_style('fontawesome'); 
    wp_deregister_style('animate'); 
    wp_deregister_style('font-electro'); 
    wp_deregister_style('electro-style'); 
    wp_deregister_style('electro-style-v2'); 
    wp_deregister_style('electro-child-style'); 
    wp_deregister_style('ywpar_frontend'); 
    wp_deregister_style('ivpa-style'); 
    wp_deregister_style('popup-maker-site'); 
    wp_deregister_style('electro-custom-color'); 
    wp_deregister_style('wc-featured-product-editor');
    wp_deregister_style('wc-vendors');
    wp_deregister_style('js_composer_front');
    wp_deregister_style('prettyphoto');
    wp_deregister_style('tc-epo-css');
    wp_deregister_style('photoswipe');
    wp_deregister_style('photoswipe-default-skin');
}
add_action('wp_enqueue_scripts', 'remove_css', 9999);
add_action ('wp_print_styles','remove_css',100);


/*
 *  Acync CSS loading
 */

function print_css_async() {

    // make a stylesheet link
    $return = '<script type="text/javascript">';
    $return .= 'var myCSS = document.createElement( "link" );';
    $return .= 'myCSS.rel = "stylesheet";';
    $return .= 'myCSS.href = "https://bormashop.ru/wp-content/themes/bormashop/style_0.css";';
    $return .= 'document.head.insertBefore( myCSS, document.head.childNodes[ document.head.childNodes.length - 1 ].nextSibling );';
    $return .= '</script>';
    $return .= '<script type="text/javascript">';
    $return .= 'var myCSS = document.createElement( "link" );';
    $return .= 'myCSS.rel = "stylesheet";';
    $return .= 'myCSS.href = "https://bormashop.ru/wp-content/themes/bormashop/style_1.css";';
    $return .= 'document.head.insertBefore( myCSS, document.head.childNodes[ document.head.childNodes.length - 1 ].nextSibling );';
    $return .= '</script>';

    echo $return;
}
//add_action( 'wp_head', 'print_css_async', 2 );

/*
 *  Add critical CSS
 */

function bs_critical_css_method() {
    $custom_css = '<style>';
    //$custom_css .= file_get_contents('critical_css1.css', true);
    $custom_css .= file_get_contents('style_0.css', true);
    $custom_css .= file_get_contents('style_1.css', true);
    $custom_css .= '</style>';
    echo $custom_css;
}

add_action( 'wp_head', 'bs_critical_css_method', 0 );


function show_product_file() {
    $output = '<div class="product_file">';
    $file_url = get_field('product_file');
    if(!empty($file_url)) {
        $output .= '<a target="_blank" href="'.$file_url.'"><img alt="Скачать спецификацию" src="/wp-content/themes/bormashop/img/pdf-file.svg" width="39" /> <span>Скачать спецификацию</span></a>';
    }
    $output .= '</div>';
    echo $output;
}

add_action( 'woocommerce_before_add_to_cart_form',    'show_product_file', 11 );
  


function change_theme($current_theme) {
    $user_agent = $_SERVER['HTTP_USER_AGENT']; 
    if(strpos( $user_agent, 'Lighthouse') !== false) {
        return 'bormashop-mobile';
    } else {
        return $current_theme;
    }
}
//add_filter( 'stylesheet', 'change_theme' );
//add_filter( 'template', 'change_theme' );
//add_filter('option_template', 'change_theme');
//add_filter('option_stylesheet', 'change_theme');

/**
 * Remove shortcodes if we are on some specific page
 */
function prefix_remove_shortcode_trigger_on_specific_pages() {
     $user_agent = $_SERVER['HTTP_USER_AGENT']; 
    if(strpos( $_SERVER['HTTP_USER_AGENT'], 'Lighthouse') !== false) {
        remove_shortcode('rev_slider');
    } 
}
 
//add_action( 'init', 'prefix_remove_shortcode_trigger_on_specific_pages');

function is_lighthouse1() {
     $user_agent = $_SERVER['HTTP_USER_AGENT']; 
    if(strpos( $_SERVER['HTTP_USER_AGENT'], 'Lighthouse') !== false) {
        return true;
    }  
}