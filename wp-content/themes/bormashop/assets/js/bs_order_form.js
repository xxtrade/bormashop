(function($, window){

	$(document).ready(function(){
		$("#billing_first_name").on("keyup", function(){
			$(this).val(first_letter($(this).val()));
		}).focusout(function(){
			$(this).val(first_letter($(this).val()));
		});	
		$("#billing_last_name").on("keyup", function(){
			$(this).val(first_letter($(this).val()));
		}).focusout(function(){
			$(this).val(first_letter($(this).val()));
		});
		$("#account_first_name").on("keyup", function(){
			$(this).val(first_letter($(this).val()));
		}).focusout(function(){
			$(this).val(first_letter($(this).val()));
		});	
		$("#account_last_name").on("keyup", function(){
			$(this).val(first_letter($(this).val()));
		}).focusout(function(){
			$(this).val(first_letter($(this).val()));
		});			
	});

	var first_letter = function(text){
		var new_text = text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
		return new_text;
	};

})(jQuery);