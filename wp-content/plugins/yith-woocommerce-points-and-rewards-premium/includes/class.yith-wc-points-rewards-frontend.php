<?php

if ( ! defined( 'ABSPATH' ) || ! defined( 'YITH_YWPAR_VERSION' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Implements features of YITH WooCommerce Points and Rewards Frontend
 *
 * @class   YITH_WC_Points_Rewards_Frontend
 * @package YITH WooCommerce Points and Rewards
 * @since   1.0.0
 * @author  YITH
 */
if ( ! class_exists( 'YITH_WC_Points_Rewards_Frontend' ) ) {

	/**
	 * Class YITH_WC_Points_Rewards_Frontend
	 */
	class YITH_WC_Points_Rewards_Frontend {

		/**
		 * Single instance of the class
		 *
		 * @var \YITH_WC_Points_Rewards_Frontend
		 */
		protected static $instance;

		/**
		 * Returns single instance of the class
		 *
		 * @return \YITH_WC_Points_Rewards_Frontend
		 * @since 1.0.0
		 */
		public static function get_instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * Initialize plugin and registers actions and filters to be used.
		 *
		 * @since  1.0.0
		 * @author Emanuela Castorina
		 */
		public function __construct() {

			//add shortcodes on my account
			add_shortcode( 'ywpar_my_account_points', array( $this, 'shortcode_my_account_points' ) );

			//Disable plugin when it is disabled by option
			if ( ! YITH_WC_Points_Rewards()->is_enabled() ) {
				return;
			}

			//Hide messages of points for guests if option is enabled
			//APPLY_FILTER : ywpar_hide_messages: filtering the hiding messages to guests
			if ( apply_filters( 'ywpar_hide_messages', false ) || ( YITH_WC_Points_Rewards()->get_option( 'hide_point_system_to_guest' ) == 'yes' && ! is_user_logged_in() ) ) {
				return;
			}

			//From here the plugin is active

			//Add shortcode to show messages in single product page
			add_shortcode( 'yith_points_product_message', array( $this, 'show_single_product_message' ) );


			add_action( 'init', array( $this, 'init' ), 5 );

			//if the user is not enabled to and the option to a the option to hide points to guest is enabled (no filter at this time) don't show the messages
			if ( ( is_user_logged_in() && ! YITH_WC_Points_Rewards()->is_user_enabled() ) || ( ! is_user_logged_in() && YITH_WC_Points_Rewards()->get_option( 'hide_point_system_to_guest' ) == 'yes' ) ) {
				return;
			}

			//Add messages on cart or checkout if them are enabled
			add_action( 'template_redirect', array( $this, 'show_messages' ), 30 );

			//check if the messages on cart are enabled for ajax calls
			if ( YITH_WC_Points_Rewards()->get_option( 'enabled_cart_message' ) == 'yes' ) {
				add_action( 'wc_ajax_ywpar_update_cart_messages', array( $this, 'print_cart_message' ) );
			}
		}


		/**
		 * Show messages on cart or checkout page if the options are enabled
		 */
		public function show_messages() {

			//APPLY_FILTER : ywpar_enable_points_upon_sales: Enable the store to earn or rewards points overriding the option
			if ( apply_filters( 'ywpar_enable_points_upon_sales', YITH_WC_Points_Rewards()->get_option( 'enable_points_upon_sales', 'yes' ) == 'yes' ) ) {

				//check and show messages on single product page
				if ( YITH_WC_Points_Rewards()->get_option( 'enabled_single_product_message' ) == 'yes' ) {
					$this->show_single_product_message_position();
				}

				//check and show messages on show page
				if ( YITH_WC_Points_Rewards()->get_option( 'enabled_loop_message' ) == 'yes' ) {
					$this->show_single_loop_position();
				}

				//check if the user is enabled to earn
				if ( YITH_WC_Points_Rewards()->is_user_enabled() ) {
					//check if cart messages are enabled
					if ( YITH_WC_Points_Rewards()->get_option( 'enabled_cart_message' ) == 'yes' ) {
						add_action( 'woocommerce_before_cart', array( $this, 'print_messages_in_cart' ) );
						//add_action( 'wc_ajax_ywpar_update_cart_messages', array( $this, 'print_cart_message' ) );
					}

					//check if the messages are enabled at checkout
					if ( YITH_WC_Points_Rewards()->get_option( 'enabled_checkout_message' ) == 'yes' ) {
						add_action( 'woocommerce_before_checkout_form', array( $this, 'print_messages_in_cart' ) );
						add_action( 'before_woocommerce_pay', array( $this, 'print_messages_in_cart' ) );
					}
				}
			}
		}


		/**
		 *  Add hooks when init action is triggered
		 */
		public function init() {

			//add points to earn to each variation
			add_filter( 'woocommerce_available_variation', array( $this, 'add_params_to_available_variation' ), 10, 3 );

			//enqueue scripts
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles_scripts' ) );

			/** REDEEM  */
			//check if user can redeem points and add the rewards messages if are enabled
			if ( YITH_WC_Points_Rewards()->get_option( 'enabled_rewards_cart_message' ) == 'yes' && YITH_WC_Points_Rewards()->is_user_enabled( 'redeem' ) ) {
				add_action( 'woocommerce_before_cart', array( $this, 'print_rewards_message_in_cart' ) );
				add_action( 'woocommerce_before_checkout_form', array( $this, 'print_rewards_message_in_cart' ) );
				add_action( 'wc_ajax_ywpar_update_cart_rewards_messages', array( $this, 'print_rewards_message' ) );
			}

			//exit if user is not enabled to earn
			if ( ! YITH_WC_Points_Rewards()->is_user_enabled() ) {
				return;
			}

			//Add the endpoints to WooCommerce My Account
			if ( YITH_WC_Points_Rewards()->get_option( 'show_point_list_my_account_page' ) == 'yes' ) {
				$endpoint                                  = YITH_WC_Points_Rewards()->get_option( 'my_account_page_endpoint' );
				$this->endpoint                            = ! empty( $endpoint ) ? $endpoint : 'my-points';
				WC()->query->query_vars[ $this->endpoint ] = $this->endpoint;

				add_action( 'woocommerce_account_' . $this->endpoint . '_endpoint', array( $this, 'add_endpoint' ) );
				add_filter( 'woocommerce_account_menu_items', array( $this, 'ywpar_add_points_menu_items' ), 20 );
				function_exists( 'get_home_path' ) && flush_rewrite_rules();
			}
		}

		/**
		 * Show the points My account page
		 */
		public function add_endpoint() {
			echo do_shortcode( '[ywpar_my_account_points]' );
		}

		/**
		 * Add the menu item on WooCommerce My account Menu
		 * before the Logout item menu.
		 *
		 * @param $wc_menu array
		 *
		 * @return mixed
		 */
		public function ywpar_add_points_menu_items( $wc_menu ) {

			if ( isset( $wc_menu['customer-logout'] ) ) {
				$logout = $wc_menu['customer-logout'];
				unset( $wc_menu['customer-logout'] );
			}

			$wc_menu[ $this->endpoint ] = YITH_WC_Points_Rewards()->get_option( 'my_account_page_label', __('My Points', 'yith-woocommerce-points-and-rewards') );

			if ( isset( $logout ) ) {
				$wc_menu['customer-logout'] = $logout;
			}

			return $wc_menu;
		}

		/**
		 * Enqueue Scripts and Styles
		 *
		 * @return void
		 * @since  1.0.0
		 * @author Emanuela Castorina
		 */
		public function enqueue_styles_scripts() {

			wp_enqueue_script( 'ywpar_frontend', YITH_YWPAR_ASSETS_URL . '/js/frontend' . YITH_YWPAR_SUFFIX . '.js', array( 'jquery', 'wc-add-to-cart-variation' ), YITH_YWPAR_VERSION, true );
			wp_enqueue_style( 'ywpar_frontend', YITH_YWPAR_ASSETS_URL . '/css/frontend.css' );

			$script_params = array(
				'ajax_url'    => admin_url( 'admin-ajax' ) . '.php',
				'wc_ajax_url' => WC_AJAX::get_endpoint( "%%endpoint%%" ),
			);

			wp_localize_script( 'ywpar_frontend', 'yith_wpar_general', $script_params );
		}

		/**
		 * Add message in single product page
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 *
		 * @param $atts
		 *
		 * @return string
		 */
		public function show_single_product_message( $atts ) {
			$message = '';
			$product_id = 0;
			$atts = shortcode_atts( array(
				                        'product_id' => 0,
			                        ), $atts );

			extract( $atts );
			$product_id = intval( $product_id );
			if ( ! $product_id ) {
				global $product;
			} else {
				$product = wc_get_product( $product_id );
			}

			if ( ! $product ) {
				return $message;
			}

			$message    = YITH_WC_Points_Rewards()->get_option( 'single_product_message' );
			$product_points = YITH_WC_Points_Rewards_Earning()->calculate_product_points( $product );

			if ( is_numeric( $product_points ) && $product_points <= 0 ) {
				return '';
			}

			$message = $this->replace_placeholder_on_product_message( $product, $message, $product_points );

			$class = 'hide';
			if ( $product->is_type( 'variable' ) ) {
				$message = '<div class="yith-par-message ' . esc_attr( $class ) . '">' .  $message . '</div><div class="yith-par-message-variation ' . esc_attr($class) . '">' . $message . '</div>';
			} else {
				$message ='<div class="yith-par-message">' . $message . '</div>';
			}

			//APPLY_FILTER : ywpar_point_message_single_page: filtering the point message on single product page
			return apply_filters('ywpar_point_message_single_page', $message,  $product, $class );
		}

		/**
		 * Print single product message.
		 *
		 * @author Francesco Licandro
		 */
		public function print_single_product_message() {
			echo do_shortcode( '[yith_points_product_message]' );
		}

		/**
		 * Set the position where display the message in single product
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 * @return  void
		 */
		public function show_single_product_message_position() {

			$position                = YITH_WC_Points_Rewards()->get_option( 'single_product_message_position' );
			$priority                = 10;
			$action                  = '';
			$priority_single_excerpt = has_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt' );
			$priority_after_meta     = has_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta' );
			switch ( $position ) {
				case 'before_add_to_cart':
					$action = 'woocommerce_before_add_to_cart_form';
					break;
				case 'after_add_to_cart':
					$action = 'woocommerce_after_add_to_cart_form';
					break;
				case 'before_excerpt':
					$action   = 'woocommerce_single_product_summary';
					$priority = $priority_single_excerpt ? $priority_single_excerpt - 1 : 18;
					break;
				case 'after_excerpt':
					$action   = 'woocommerce_single_product_summary';
					$priority = $priority_single_excerpt ? $priority_single_excerpt - 1 : 22;
					break;
				case 'after_meta':
					$action   = 'woocommerce_single_product_summary';
					$priority = $priority_after_meta ? $priority_after_meta + 1 : 42;
					break;
				default:
					break;
			}

			//APPLY_FILTER : ywpar_show_single_product_message_position_action: filtering action where show the message in single product page
			$action   = apply_filters( 'ywpar_show_single_product_message_position_action', $action, $position, $priority );
			//APPLY_FILTER : ywpar_show_single_product_message_position_priority: filtering the priority where show the message in single product page
			$priority = apply_filters( 'ywpar_show_single_product_message_position_priority', $priority, $position, $action );

			add_action( $action, array( $this, 'print_single_product_message' ), $priority );
		}

		/**
		 * Set the position where display the message in loop
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 * @return  void
		 */
		public function show_single_loop_position() {
			//APPLY_FILTER : ywpar_loop_position: filtering the position where show the message in the loop
			$position = apply_filters( 'ywpar_loop_position', 'woocommerce_after_shop_loop_item_title' );
			//APPLY_FILTER : ywpar_loop_position: filtering the priority where show the message in the loop
			$priority = apply_filters( 'ywpar_loop_position_priority', 11 );
			add_action( $position, array( $this, 'print_messages_in_loop' ), $priority );
		}

		/**
		 * Print a message in loop
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 * @return  void
		 */
		public function print_messages_in_loop() {
			global $product;
			$message        = YITH_WC_Points_Rewards()->get_option( 'loop_message' );
			$product_points = YITH_WC_Points_Rewards_Earning()->calculate_product_points( $product );

			$message = $this->replace_placeholder_on_product_message( $product, $message, $product_points, true );
			//APPLY_FILTER : ywpar_single_product_message_in_loop: filtering the message in loop
			echo apply_filters( 'ywpar_single_product_message_in_loop', '<div  class="yith-par-message">' . $message . '</div>', $product, $product_points );
		}

		/**
		 * Return the message with the placeholder replaced.
		 *
		 * @param $product WC_Product
		 * @param $message string
		 * @param $product_points int
		 *
		 * @return mixed
		 */
		private function replace_placeholder_on_product_message( $product, $message, $product_points, $loop = false ) {
			$singular         = YITH_WC_Points_Rewards()->get_option( 'points_label_singular' );
			$plural           = YITH_WC_Points_Rewards()->get_option( 'points_label_plural' );
			$class_name       = $loop ? 'product_point_loop' : 'product_point';
			$product_discount = ( 'fixed' == YITH_WC_Points_Rewards_Redemption()->get_conversion_method() ) ? YITH_WC_Points_Rewards_Redemption()->calculate_price_worth( $product, $product_points ) : '';

			//replace {points} placeholder
			$message = str_replace( '{points}', '<span class="' . esc_attr( $class_name ) . '">' . $product_points . '</span>', $message );

			//replace {price_discount_fixed_conversion} placeholder
			$message = empty( $product_discount ) ? str_replace( '{price_discount_fixed_conversion}', '', $message ) : str_replace( '{price_discount_fixed_conversion}', '<span class="product-point-conversion">' . $product_discount . '</span>', $message );

			//replace {points_label} placeholder
			$message = str_replace( '{points_label}', ( $product_points > 1 ? $plural : $singular ), $message );

			return $message;
		}

		/**
		 * Print a message in cart/checkout page or in my account pay order page.
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 * @return  void
		 */
		public function print_messages_in_cart() {

			$points_earned = false;

			if ( isset( $_GET['key'] ) ) {
				$order_id = wc_get_order_id_by_order_key( $_GET['key'] );
				if ( $order_id ) {
					$points_earned = get_post_meta( $order_id, 'ywpar_points_from_cart', true );
				}
			}

			$message = $this->get_cart_message( $points_earned );

			if ( ! empty( $message ) ) {
				//APPLY_FILTER : yith_par_messages_class: filtering the classes of messages in cart/checkout
				$yith_par_message_classes = apply_filters( 'yith_par_messages_class', array(
					'woocommerce-cart-notice',
					'woocommerce-cart-notice-minimum-amount',
					'woocommerce-info'
				) );
				$classes                  = count( $yith_par_message_classes ) > 0 ? implode( ' ', $yith_par_message_classes ) : '';
				printf( '<div id="yith-par-message-cart" class="%s">%s</div>', esc_attr( $classes ),  $message  );
			}
		}

		/**
		 * Return the message to show on cart or checkout for point to earn.
		 *
		 * @since   1.1.3
		 * @author  Andrea Frascaspata
		 *
		 * @param int $total_points
		 *
		 * @return string
		 */
		private function get_cart_message( $total_points = 0 ) {

			$page = is_checkout() ? 'checkout' : 'cart';
			$message  = YITH_WC_Points_Rewards()->get_option( $page . '_message' );
			$singular = YITH_WC_Points_Rewards()->get_option( 'points_label_singular' );
			$plural   = YITH_WC_Points_Rewards()->get_option( 'points_label_plural' );

			if ( $total_points == 0 ) {
				$total_points = YITH_WC_Points_Rewards_Earning()->calculate_points_on_cart();
				if ( $total_points == 0 ) {
					return '';
				}
			}

			$conversion_method = YITH_WC_Points_Rewards_Redemption()->get_conversion_method();

			if ( $conversion_method == 'fixed' ) {
				$conversion  = YITH_WC_Points_Rewards_Redemption()->get_conversion_rate_rewards();
				$point_value = $conversion['money'] / $conversion['points'];
				$discount    = $total_points * $point_value;
			}

			$message = str_replace( '{points}', $total_points, $message );
			$message = str_replace( '{points_label}', ( $total_points > 1  ) ? $plural : $singular, $message );
			$message = str_replace( '{price_discount_fixed_conversion}',  isset( $discount ) ? wc_price( $discount ) : '', $message) ;

			return $message;

		}

		/**
		 * Print the cart message on ajax call the the cart is updated.
		 *
		 * @since   1.1.3
		 * @author  Andrea Frascaspata
		 */
		public function print_cart_message() {
			echo $this->get_cart_message();
			die;
		}

		/**
		 * Print rewards message in cart/checkout page
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 * @return  void
		 */
		public function print_rewards_message_in_cart() {

			$coupons = WC()->cart->get_applied_coupons();

			//the message will not showed if the coupon is just applied to cart
			if ( YITH_WC_Points_Rewards_Redemption()->check_coupon_is_ywpar( $coupons ) ) {
				return '';
			}

			$message = $this->get_rewards_message();

			if ( $message ) {
				//APPLY_FILTER : yith_par_messages_class: filtering the classes of messages in cart/checkout
				$yith_par_message_classes = apply_filters( 'yith_par_messages_class', array(
					                                                                    'woocommerce-cart-notice',
					                                                                    'woocommerce-cart-notice-minimum-amount',
					                                                                    'woocommerce-info'
				                                                                    )
				);
				$classes                  = count( $yith_par_message_classes ) > 0 ? implode( ' ', $yith_par_message_classes ) : '';
				printf( '<div id="yith-par-message-reward-cart" class="%s">%s</div>', $classes, $message );
			}

		}

		/**
		 * @since   1.1.3
		 * @author  Andrea Frascaspata
		 * @return mixed|string|void
		 */
		private function get_rewards_message() {
			//DO_ACTION : ywpar_before_rewards_message : action triggered before the rewards message
			do_action( 'ywpar_before_rewards_message' );
			$message = '';

			if ( is_user_logged_in() ) {

				$message                 = YITH_WC_Points_Rewards()->get_option( 'rewards_cart_message' );
				$plural                  = YITH_WC_Points_Rewards()->get_option( 'points_label_plural' );
				$max_discount            = YITH_WC_Points_Rewards_Redemption()->calculate_rewards_discount();
				$minimum_amount          = YITH_WC_Points_Rewards()->get_option( 'minimum_amount_to_redeem' );
				$max_percentual_discount = YITH_WC_Points_Rewards_Redemption()->get_max_percentual_discount();

				//APPLY_FILTER : ywpar_hide_value_for_max_discount: hide the message if $max_discount is < 0
				$max_discount_2 = apply_filters( 'ywpar_hide_value_for_max_discount', $max_discount );


				if ( ! empty( $minimum_amount ) && WC()->cart->subtotal < $minimum_amount ) {
					return;
				}

				if ( $max_discount > 0 ) {

					$max_points = YITH_WC_Points_Rewards_Redemption()->get_max_points();
					if ( $max_points == 0 ) {
						return;
					}

					if ( YITH_WC_Points_Rewards()->get_option( 'conversion_rate_method' ) == 'fixed' ) {

						$minimum_discount_amount = YITH_WC_Points_Rewards()->get_option( 'minimum_amount_discount_to_redeem' );


						if ( ! empty( $minimum_discount_amount ) && $max_discount < $minimum_discount_amount ) {
							return '';
						}


						$message = str_replace( '{points_label}', $plural, $message );
						$message = str_replace( '{max_discount}', wc_price( $max_discount ), $message );
						$message = str_replace( '{points}', $max_points, $message );
						$message .= ' <a class="ywpar-button-message">' . YITH_WC_Points_Rewards()->get_option( 'label_apply_discounts' ) . '</a>';
						$message .= '<div class="clear"></div><div class="ywpar_apply_discounts_container"><form class="ywpar_apply_discounts" method="post">' . wp_nonce_field( 'ywpar_apply_discounts', 'ywpar_input_points_nonce' ) . '
                                    <input type="hidden" name="ywpar_points_max" value="' . $max_points . '">
                                    <input type="hidden" name="ywpar_max_discount" value="' . $max_discount_2 . '">
                                    <input type="hidden" name="ywpar_rate_method" value="fixed">
                                    <p class="form-row form-row-first">
                                        <input type="text" name="ywpar_input_points" class="input-text"  id="ywpar-points-max" value="' . $max_points . '">
                                        <input type="hidden" name="ywpar_input_points_check" id="ywpar_input_points_check" value="0">
                                    </p>
                                    <p class="form-row form-row-last">
                                        <input type="submit" class="button" name="ywpar_apply_discounts" id="ywpar_apply_discounts" value="' . YITH_WC_Points_Rewards()->get_option( 'label_apply_discounts' ) . '">
                                    </p>
                                    <div class="clear"></div>
                                </form></div>';


					} elseif ( YITH_WC_Points_Rewards()->get_option( 'conversion_rate_method' ) == 'percentage' ) {

						$message = str_replace( '{points_label}', $plural, $message );
						$message = str_replace( '{max_discount}', wc_price( $max_discount ), $message );
						$message = str_replace( '{max_percentual_discount}', $max_percentual_discount . '%', $message );
						$message = str_replace( '{points}', $max_points, $message );
						$message .= ' <a class="ywpar-button-message ywpar-button-percentage-discount">' . YITH_WC_Points_Rewards()->get_option( 'label_apply_discounts' ) . '</a>';
						$message .= '<div class="ywpar_apply_discounts_container"><form class="ywpar_apply_discounts" method="post">' . wp_nonce_field( 'ywpar_apply_discounts', 'ywpar_input_points_nonce' ) . '
                                     <input type="hidden" name="ywpar_points_max" value="' . $max_points . '">
                                     <input type="hidden" name="ywpar_max_discount" value="' . $max_discount_2 . '">
                                     <input type="hidden" name="ywpar_rate_method" value="percentage">';

						$message .= '</form></div>';


					}

				} else {
					$message = '';
				}

				//DO_ACTION : ywpar_after_rewards_message : action triggered after the rewards message
				do_action( 'ywpar_after_rewards_message' );
			}

			return $message;
		}

		/**
		 * @since   1.1.3
		 * @author  Andrea Frascaspata
		 */
		public function print_rewards_message() {

			echo $this->get_rewards_message();

		}

		/**
		 * Shortcode my account points
		 *
		 * @since  1.1.3
		 * @author Francesco Licandro
		 * @return string
		 */
		public function shortcode_my_account_points() {
			if ( ! YITH_WC_Points_Rewards()->is_enabled() || ! YITH_WC_Points_Rewards()->is_user_enabled() ) {
				return '';
			}

			ob_start();
			wc_get_template( '/myaccount/my-points-view.php', null, '', YITH_YWPAR_TEMPLATE_PATH );

			return ob_get_clean();
		}

		/**
		 * Add points section to my-account page
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 * @return  void
		 */
		public function my_account_points() {
			echo do_shortcode( '[ywpar_my_account_points]' );
		}

		/**
		 * Add custom params to variations
		 *
		 * @access public
		 *
		 * @param $args      array
		 * @param $product   object
		 * @param $variation object
		 *
		 * @return array
		 * @since  1.1.1
		 */
		public function add_params_to_available_variation( $args, $product, $variation ) {

			if ( $variation ) {
				$args['variation_points']                          = YITH_WC_Points_Rewards_Earning()->calculate_product_points( $variation );
				$args['variation_price_discount_fixed_conversion'] = YITH_WC_Points_Rewards_Redemption()->calculate_price_worth( $variation->get_id(), $args['variation_points'] );
			}

			return $args;
		}

	}


}

/**
 * Unique access to instance of YITH_WC_Points_Rewards_Frontend class
 *
 * @return \YITH_WC_Points_Rewards_Frontend
 */
function YITH_WC_Points_Rewards_Frontend() {
	return YITH_WC_Points_Rewards_Frontend::get_instance();
}

