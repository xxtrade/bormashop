<?php
$request_uri = parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH );

$is_admin = strpos( $request_uri, '/wp-admin/' );

// add filter in front pages only
if( false === $is_admin ){
	add_filter( 'option_active_plugins', 'kinsta_option_active_plugins' );
}

function kinsta_option_active_plugins( $plugins ){
	global $request_uri;
	$is_contact_page = strpos( $request_uri, '/contact/' );

	$unnecessary_plugins = array();

	// conditions
	// if this is not contact page
	// deactivate plugin
	if( false === $is_contact_page ){
		$unnecessary_plugins[] = 'contact-form-7/wp-contact-form-7.php';
	}

	foreach ( $unnecessary_plugins as $plugin ) {
		$k = array_search( $plugin, $plugins );
		if( false !== $k ){
			unset( $plugins[$k] );
		}
	}
	return $plugins;
}

