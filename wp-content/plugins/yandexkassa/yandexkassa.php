<?php
/**
 * Plugin Name: Яндекс.Касса для WooCommerce
 * Plugin URI: https://wordpress.org/plugins/yandexkassa/
 * Description: Платежный модуль для работы с сервисом Яндекс.Касса через плагин WooCommerce
 * Version: 2.3.11
 * Author: Yandex.Money
 * Author URI: http://kassa.yandex.ru
 * License URI: https://money.yandex.ru/doc.xml?id=527132
 */
if (!defined('ABSPATH')) {
    exit;
}
include_once 'yandexkassa_gateway.class.php';
define("YAMONEY_VERSION", '2.3.11');

function yamoney_all_gateway_icon($gateways)
{
    $list_icons = array(
        'kassa'           => 'kassa',
        'yandex_money'    => 'pc',
        'bank'            => 'ac',
        'terminal'        => 'gp',
        'mobile'          => 'mc',
        'yandex_webmoney' => 'wm',
        'alfabank'        => 'ab',
        'sberbank'        => 'sb',
        'masterpass'      => 'ma',
        'psbank'          => 'pb',
        'qiwi'            => 'qw',
        'credit'          => 'cr',
        'mpos'            => 'ac',
    );
    $url        = plugins_url('images/', __FILE__);
    foreach ($list_icons as $name => $png_name) {
        if (isset($gateways[$name])) {
            $gateways[$name]->icon = $url.$png_name.'.png';
        }
    }

    return $gateways;
}

add_filter('woocommerce_available_payment_gateways', 'yamoney_all_gateway_icon');

if (!class_exists('WC_yamoney_Gateway')) {
    return;
}

class WC_yamoney_smartpay extends WC_yamoney_smartpay_Gateway
{
    public function __construct()
    {
        $this->id           = 'kassa';
        $this->method_title = 'Яндекс.Касса (банковские карты, электронные деньги и другое)';
        $this->long_name    = '';
        $this->payment_type = '';
        parent::__construct();
    }
}

class WC_yamoney_PC extends WC_yamoney_Gateway
{
    public function __construct()
    {
        $this->id           = 'yandex_money';
        $this->method_title = 'Яндекс.Деньги';
        $this->long_name    = '';
        $this->payment_type = 'PC';
        parent::__construct();
    }
}

class WC_yamoney_AC extends WC_yamoney_Gateway
{
    public function __construct()
    {
        $this->id           = 'bank';
        $this->method_title = 'Банковские карты';
        $this->long_name    = '';
        $this->payment_type = 'AC';
        parent::__construct();
    }
}

class WC_yamoney_GP extends WC_yamoney_Gateway
{
    public function __construct()
    {
        $this->id           = 'terminal';
        $this->method_title = 'Наличные через терминалы';
        $this->long_name    = '';
        $this->payment_type = 'GP';
        parent::__construct();
    }
}

class WC_yamoney_MC extends WC_yamoney_Gateway
{
    public function __construct()
    {
        $this->id           = 'mobile';
        $this->method_title = 'Счёт мобильного';
        $this->long_name    = '';
        $this->payment_type = 'MC';
        parent::__construct();
    }
}

class WC_yamoney_WM extends WC_yamoney_Gateway
{
    public function __construct()
    {
        $this->id           = 'yandex_webmoney';
        $this->method_title = 'WebMoney';
        $this->long_name    = '';
        $this->payment_type = 'WM';
        parent::__construct();
    }
}

class WC_yamoney_AB extends WC_yamoney_Gateway
{
    public function __construct()
    {
        $this->id           = 'alfabank';
        $this->method_title = 'Альфа-Клик';
        $this->long_name    = '';
        $this->payment_type = 'AB';
        parent::__construct();
    }
}

class WC_yamoney_SB extends WC_yamoney_Gateway
{
    public function __construct()
    {
        $this->id           = 'sberbank';
        $this->method_title = 'Сбербанк Онлайн';
        $this->long_name    = '';
        $this->payment_type = 'SB';
        parent::__construct();
    }
}

class WC_yamoney_PB extends WC_yamoney_Gateway
{
    public function __construct()
    {
        $this->id           = 'psbank';
        $this->method_title = 'Промсвязьбанк';
        $this->long_name    = '';
        $this->payment_type = 'PB';
        parent::__construct();
    }
}

class WC_yamoney_QW extends WC_yamoney_Gateway
{
    public function __construct()
    {
        $this->id           = 'qiwi';
        $this->method_title = 'QIWI Wallet';
        $this->long_name    = '';
        $this->payment_type = 'QW';
        parent::__construct();
    }
}

class WC_yamoney_CR extends WC_yamoney_Gateway
{
    public function __construct()
    {
        $this->id           = 'credit';
        $this->method_title = 'Заплатить по частям';
        $this->long_name    = '';
        $this->payment_type = 'CR';
        parent::__construct();
    }
}

class WC_yamoney_MA extends WC_yamoney_Gateway
{
    public function __construct()
    {
        $this->id           = 'masterpass';
        $this->method_title = 'MasterPass';
        $this->long_name    = '';
        $this->payment_type = 'MA';
        parent::__construct();
    }
}

class WC_yamoney_2S extends WC_yamoney_Gateway
{
    const VAT_TYPE_UNTAXED = 'untaxed';
    const VAT_TYPE_CALCULATED = 'calculated';

    public function __construct()
    {
        $this->id           = 'sbbol';
        $this->method_title = 'Сбербанк Бизнес Онлайн';
        $this->long_name    = '';
        $this->payment_type = '2S';
        parent::__construct();
    }

    public function generate_payu_form($order_id)
    {
        global $woocommerce;
        $this->order = new WC_Order($order_id);
        $order       = $this->order;


        $data                       = $order->get_data();
        $paymentPurposeTemplateData = array();
        foreach ($order as $key => $value) {
            if (is_scalar($value)) {
                $paymentPurposeTemplateData['%'.$key.'%'] = $value;
            }
        }

        $items       = $order->get_items();
        $shipping    = $data['shipping_lines'];
        $hasShipping = (bool)count($shipping);
        $sbbolTaxes  = array();

        if (version_compare($woocommerce->version, "3.0", ">=")) {
            $billing_first_name = $this->order->get_billing_first_name();
            $billing_last_name  = $this->order->get_billing_last_name();
            $order_total        = $order->get_total();
        } else {
            $billing_first_name = $this->order->billing_first_name;
            $billing_last_name  = $this->order->billing_last_name;
            $order_total        = number_format($order->order_total, 2, '.', '');
        }

        foreach ($items as $item) {
            $taxes        = $item->get_taxes();
            $sbbolTaxes[] = $this->getSbbolTaxRate($taxes);
        }

        if ($hasShipping) {
            $shippingData = array_shift($shipping);
            $taxes        = $shippingData->get_taxes();
            $sbbolTaxes[] = $this->getSbbolTaxRate($taxes);
        }

        $sbbolTaxes = array_unique($sbbolTaxes);

        if (count($sbbolTaxes) !== 1) {
            return 'У вас в корзине товары, для которых действуют разные ставки НДС — их нельзя оплатить одновременно. Можно разбить покупку на несколько этапов: сначала оплатить товары с одной ставкой НДС, потом — с другой.';
        }

        $vatType = reset($sbbolTaxes);

        if ($vatType !== self::VAT_TYPE_UNTAXED) {
            $vatRate = $vatType;
            $vatType = self::VAT_TYPE_CALCULATED;
            $vatSum  = $order_total * $vatRate / 100;
        }


        $paymentPurposeTemplate = get_option('ym_sbbol_purpose');
        $paymentPurpose         = strtr($paymentPurposeTemplate, $paymentPurposeTemplateData);

        $txnid   = $order_id;
        $sendurl = get_option('ym_Demo') == '1' ? 'https://demomoney.yandex.ru/eshop.xml' : 'https://money.yandex.ru/eshop.xml';
        $result  = '';
        $result  .= '<form name=ShopForm method="POST" id="submit_'.$this->id.'_payment_form" action="'.$sendurl.'">';
        $result  .= '<input type="hidden" name="firstname" value="'.$billing_first_name.'">';
        $result  .= '<input type="hidden" name="lastname" value="'.$billing_last_name.'">';
        $result  .= '<input type="hidden" name="scid" value="'.get_option('ym_Scid').'">';
        $result  .= '<input type="hidden" name="shopId" value="'.get_option('ym_ShopID').'"> ';
        $result  .= '<input type="hidden" name="shopSuccessUrl" value="'.$this->get_success_fail_url('ym_success').'"> ';
        $result  .= '<input type="hidden" name="shopFailUrl" value="'.$this->get_success_fail_url('ym_fail').'"> ';
        $result  .= '<input type="hidden" name="CustomerNumber" value="'.$txnid.'" size="43">';

        $result .= '<input type="hidden" name="payment_purpose" value="'.$paymentPurpose.'">';
        $result .= '<input type="hidden" name="vatType" value="'.$vatType.'">';
        if ($vatType == self::VAT_TYPE_CALCULATED) {
            $result .= '<input type="hidden" name="vatRate" value="'.$vatRate.'">';
            $result .= '<input type="hidden" name="vatSum" value="'.$vatSum.'">';
        }

        $result .= '<input type="hidden" name="sum" value="'.$order_total.'">';
        $result .= '<input name="paymentType" value="'.$this->payment_type.'" type="hidden">';
        $result .= '<input name="cms_name" type="hidden" value="wp-woocommerce">';
        $result .= '<input type="submit" value="Оплатить">';
        $result .= '<script type="text/javascript">';
        $result .= 'jQuery(document).ready(function ($){ jQuery("#submit_'.$this->id.'_payment_form").submit(); });';
        $result .= '</script></form>';
        $woocommerce->cart->empty_cart();

        return $result;
    }

    private function getSbbolTaxRate($taxes)
    {
        $taxRatesRelations = get_option('ym_sbbol_tax_rate');
        $defaultTaxRate    = get_option('ym_sbbol_default_tax_rate');
        if ($taxRatesRelations) {
            $taxesSubtotal = $taxes['total'];

            if ($taxesSubtotal) {
                $wcTaxIds = array_keys($taxesSubtotal);
                $wcTaxId  = $wcTaxIds[0];
                if (isset($taxRatesRelations[$wcTaxId])) {
                    return $taxRatesRelations[$wcTaxId];
                }
            }
        }

        return $defaultTaxRate;
    }
}

if (!class_exists('WC_yamoney_mpos_Gateway')) {
    return;
}

class WC_yamoney_MP extends WC_yamoney_mpos_Gateway
{
    public function __construct()
    {
        $this->id           = 'mpos';
        $this->method_title = 'Оплата картой при доставке';
        $this->long_name    = '';
        $this->payment_type = 'MP';
        parent::__construct();
    }
}

function yamoney_add_all_gateway($methods)
{
    if (get_option('ym_paymode') == '1') {
        $methods[] = 'WC_yamoney_smartpay';
    } else {
        $methods[] = 'WC_yamoney_PC';
        $methods[] = 'WC_yamoney_AC';
        $methods[] = 'WC_yamoney_GP';
        $methods[] = 'WC_yamoney_MC';
        $methods[] = 'WC_yamoney_WM';
        $methods[] = 'WC_yamoney_AB';
        $methods[] = 'WC_yamoney_SB';
        $methods[] = 'WC_yamoney_MA';
        $methods[] = 'WC_yamoney_PB';
        $methods[] = 'WC_yamoney_QW';
        $methods[] = 'WC_yamoney_CR';
        $methods[] = 'WC_yamoney_MP';
    }

    if (get_option('ym_enable_sbbol') == 'on') {
        $methods[] = 'WC_yamoney_2S';
    }

    return $methods;
}

add_filter('woocommerce_payment_gateways', 'yamoney_add_all_gateway');

function register_yamoney_setting()
{
    register_setting('woocommerce-yamoney', 'ym_Scid');
    register_setting('woocommerce-yamoney', 'ym_ShopID');
    register_setting('woocommerce-yamoney', 'ym_shopPassword');
    register_setting('woocommerce-yamoney', 'ym_Demo');

    register_setting('woocommerce-yamoney', 'ym_enable_sbbol');
    register_setting('woocommerce-yamoney', 'ym_sbbol_tax_rate');
    register_setting('woocommerce-yamoney', 'ym_sbbol_default_tax_rate');
    register_setting('woocommerce-yamoney', 'ym_sbbol_purpose');


    register_setting('woocommerce-yamoney', 'ym_paymode');
    register_setting('woocommerce-yamoney', 'ym_page_mpos');
    register_setting('woocommerce-yamoney', 'ym_success');
    register_setting('woocommerce-yamoney', 'ym_fail');
    register_setting('woocommerce-yamoney', 'ym_tax_rates_enum');
    register_setting('woocommerce-yamoney', 'ym_enable_receipt');
    register_setting('woocommerce-yamoney', 'ym_default_tax_rate');
    register_setting('woocommerce-yamoney', 'ym_tax_rate');
    register_setting('woocommerce-yamoney', 'ym_payment_subject_default');
    register_setting('woocommerce-yamoney', 'ym_payment_mode_default');

    update_option('ym_tax_rates_enum', array(
        1 => "Не облагается",
        2 => "0%",
        3 => "10%",
        4 => "18%",
        5 => "Расчетная ставка",
        6 => "Расчетная ставка 18/118",
    ));

    update_option('ym_sbbol_tax_rates_enum', array(
        'untaxed' => 'Без НДС',
        '7'       => '7%',
        '10'      => '10%',
        '18'      => '18%',
    ));
}

add_action('admin_menu', 'register_yandexMoney_submenu_page');
add_action('update_option_ym_ShopID', 'after_update_setting');
function register_yandexMoney_submenu_page()
{
    add_submenu_page('woocommerce', 'Настройки Яндекс.Кассы', 'Настройки Яндекс.Кассы', 'manage_options',
        'yandex_money_menu', 'yandexMoney_submenu_page_callback');
    add_action('admin_init', 'register_yamoney_setting');
}


function yandexMoney_submenu_page_callback()
{
    $paymentModeEnum = array(
        'full_prepayment'    => 'Полная предоплата (full_prepayment)',
        'partial_prepayment' => 'Частичная предоплата (partial_prepayment)',
        'advance'            => 'Аванс (advance)',
        'full_payment'       => 'Полный расчет (full_payment)',
        'partial_payment'    => 'Частичный расчет и кредит (partial_payment)',
        'credit'             => 'Кредит (credit)',
        'credit_payment'     => 'Выплата по кредиту (credit_payment)',
    );

    $paymentSubjectEnum = array(
        'commodity'             => 'Товар (commodity)',
        'excise'                => 'Подакцизный товар (excise)',
        'job'                   => 'Работа (job)',
        'service'               => 'Услуга (service)',
        'gambling_bet'          => 'Ставка в азартной игре (gambling_bet)',
        'gambling_prize'        => 'Выигрыш в азартной игре (gambling_prize)',
        'lottery'               => 'Лотерейный билет (lottery)',
        'lottery_prize'         => 'Выигрыш в лотерею (lottery_prize)',
        'intellectual_activity' => 'Результаты интеллектуальной деятельности (intellectual_activity)',
        'payment'               => 'Платеж (payment)',
        'agent_commission'      => 'Агентское вознаграждение (agent_commission)',
        'composite'             => 'Несколько вариантов (composite)',
        'another'               => 'Другое (another)',
    );

    $sbbolTemplate = get_option('ym_sbbol_purpose');
    if ($sbbolTemplate == '') {
        $sbbolTemplate = 'Оплата заказа № %id%';
    }
    ?>
    <div class="wrap">
        <h2>Настройки модуля Яндекс.Касса для WooCommerce</h2>
        <p>Работая с модулем, вы автоматически соглашаетесь с <a href='https://money.yandex.ru/doc.xml?id=527132'
                                                                 target='_blank'>условиями его использования</a>.</p>
        <p>Версия модуля <?php echo YAMONEY_VERSION; ?></p>
        <p>Для работы с модулем необходимо подключить магазин к <a target="_blank" href="https://kassa.yandex.ru/">Яндекс.Кассе</a>
        </p>
        <form method="post" action="options.php">
            <?php
            wp_nonce_field('update-options');
            settings_fields('woocommerce-yamoney');
            do_settings_sections('woocommerce-yamoney');
            ?>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row"></th>
                    <td>
                        <input type="radio" name="ym_Demo"
                               value="1" <?php echo get_option('ym_Demo') == '1' ? 'checked="checked"' : ''; ?>/>Тестовый
                        режим
                        <input type="radio" name="ym_Demo"
                               value="0" <?php echo get_option('ym_Demo') != '1' ? 'checked="checked"' : ''; ?>/>Рабочий
                        режим
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">checkUrl/avisoUrl</th>
                    <td><code><?php echo site_url('/?yandex_money=check', 'https'); ?></code><br>
                        <span style="line-height: 1;font-weight: normal;font-style: italic;font-size: 12px;">Скопируйте эту ссылку в поля Check URL и Aviso URL в <a
                                    target="_blank" href="https://kassa.yandex.ru/my">настройках личного кабинета Яндекс.Кассы</a><span>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">successUrl/failUrl</th>
                    <td><code>Страницы с динамическими адресами</code><br>
                        <span style="line-height: 1;font-weight: normal;font-style: italic;font-size: 12px;">Включите «Использовать страницы успеха и ошибки с динамическими адресами» в <a
                                    target="_blank" href="https://kassa.yandex.ru/my">настройках личного кабинета Яндекс.Кассы</a><span>
                    </td>
                </tr>
            </table>
            <h3>Параметры из личного кабинета Яндекс.Кассы</h3>
            <p>Shop ID, scid, shopPassword можно посмотреть в <a href='https://money.yandex.ru/my' target='_blank'>личном
                    кабинете</a> после подключения Яндекс.Кассы.</p>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Shop ID</th>
                    <td><input type="text" name="ym_ShopID" value="<?php echo get_option('ym_ShopID'); ?>"/><br/>
                        <span style="line-height: 1;font-weight: normal;font-style: italic;font-size: 12px;">
                            Идентификатор магазина<span>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row">scid</th>
                    <td><input type="text" name="ym_Scid" value="<?php echo get_option('ym_Scid'); ?>"/>
                        <br/><span style="line-height: 1;font-weight: normal;font-style: italic;font-size: 12px;">Номер витрины магазина<span>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">ShopPassword</th>
                    <td><input type="text" name="ym_shopPassword" value="<?php echo get_option('ym_shopPassword'); ?>"/>
                        <br/><span style="line-height: 1;font-weight: normal;font-style: italic;font-size: 12px;">Секретное слово<span>
                    </td>
                </tr>
            </table>
            <h3>Настройка сценария оплаты</h3>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Сценарий оплаты</th>
                    <td>
                        <input type="radio" name="ym_paymode"
                               value="1" <?php if (get_option('ym_paymode') == '1') {
                            echo ' checked="checked" ';
                        } ?> />Выбор
                        оплаты на стороне сервиса Яндекс.Касса<br>
                        <input type="radio" name="ym_paymode"
                               value="0" <?php if (get_option('ym_paymode') != '1') {
                            echo ' checked="checked" ';
                        } ?> />Выбор
                        оплаты на стороне магазина<br>
                        <a href='https://tech.yandex.ru/money/doc/payment-solution/payment-form/payment-form-docpage/'
                           target='_blank'>Подробнее о сценариях оплаты</a>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Включить платежи через Сбербанк Бизнес Онлайн</th>
                    <td>
                        <input type="checkbox" id="ym_enable_sbbol"
                               name="ym_enable_sbbol" <?php echo get_option('ym_enable_sbbol') == 'on' ? "checked" : ""; ?> >
                        <span style="line-height: 1;font-weight: normal;font-style: italic;font-size: 12px;">Если эта опция включена, вы можете принимать онлайн-платежи от юрлиц. Подробнее — <a
                                    href='https://kassa.yandex.ru' target='_blank'>на сайте Кассы</a><span>
                    </td>
                </tr>
                <tr valign="top">
                    <th></th>
                    <td>
                        <?php if (get_option('ym_enable_sbbol')): ?>

                            <?php
                            $wcTaxes             = yamoney_getAllTaxes();
                            $wcCalcTaxes         = get_option('woocommerce_calc_taxes');
                            $ymSbbolTaxRatesEnum = get_option('ym_sbbol_tax_rates_enum');
                            ?>

                            <table class="form-table">
                                <tr valign="top">
                                    <th scope="row">Шаблон для назначения платежа</th>
                                    <td><input type="text" name="ym_sbbol_purpose"
                                               value="<?php echo $sbbolTemplate; ?>"/><br/>
                                        <span style="line-height: 1;font-weight: normal;font-style: italic;font-size: 12px;">Это назначение платежа будет в платёжном поручении.<span>
                                    </td>
                                </tr>
                            </table>

                            <table class="form-table">
                                <tr valign="top">
                                    <th scope="row">Ставка по умолчанию</th>
                                    <td>
                                        <select id="ym_default_tax_rate" name="ym_sbbol_default_tax_rate">
                                            <?php foreach ($ymSbbolTaxRatesEnum as $taxId => $taxName) : ?>
                                                <option value="<?php echo $taxId ?>" <?php echo $taxId == get_option('ym_sbbol_default_tax_rate') ? 'selected=\'selected\'' : ''; ?>><?php echo $taxName ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <br/>
                                        <span style="line-height: 1;font-weight: normal;font-style: italic;font-size: 12px;">Эта ставка передаётся в Сбербанк Бизнес Онлайн, если в карточке товара не указана другая ставка. <span>
                                    </td>
                                </tr>
                            </table>

                            <?php if ($wcCalcTaxes == 'yes' && $wcTaxes) : ?>
                                <table class="form-table" style="width: 610px">
                                    <tr valign="top">
                                        <th>Сопоставьте ставки НДС в вашем магазине со ставками для
                                            Сбербанка Бизнес Онлайн
                                        </th>
                                    </tr>
                                </table>
                                <table class="form-table" style="width: 610px;">
                                    <thead>
                                    <tr valign="top">
                                        <th style="padding: 20px 10px 20px 10px;width: 110px;">Ставка НДС в вашем
                                            магазине
                                        </th>
                                        <th>Ставка НДС для Сбербанк Бизнес Онлайн</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $ymTaxes = get_option('ym_sbbol_tax_rate');
                                    ?>
                                    <?php foreach ($wcTaxes as $wcTax) : ?>
                                        <tr valign="top">
                                            <td style="vertical-align: top; text-align: left;padding: 20px 10px 20px 10px;width: 110px;line-height: 1.3;">
                                                <?php echo round($wcTax->tax_rate) ?>%
                                            </td>
                                            <td>
                                                <div>
                                                    <?php $selected = isset($ymTaxes[$wcTax->tax_rate_id]) ? $ymTaxes[$wcTax->tax_rate_id] : null; ?>
                                                    <select id="ym_sbbol_tax_rate[<?php echo $wcTax->tax_rate_id ?>]"
                                                            name="ym_sbbol_tax_rate[<?php echo $wcTax->tax_rate_id ?>]">
                                                        <?php foreach ($ymSbbolTaxRatesEnum as $taxId => $taxName) : ?>
                                                            <option value="<?php echo $taxId ?>" <?php echo $selected == $taxId ? 'selected' : ''; ?> >
                                                                <?php echo $taxName ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <br/>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row">Страница успеха платежа</th>
                    <td><select id="ym_success" name="ym_success">
                            <option value="wc_success" <?php echo((get_option('ym_success') == 'wc_success') ? ' selected' : ''); ?>>
                                Страница "Заказ принят" от WooCommerce
                            </option>
                            <option value="wc_checkout" <?php echo((get_option('ym_success') == 'wc_checkout') ? ' selected' : ''); ?>>
                                Страница оформления заказа от WooCommerce
                            </option>
                            <?php
                            if ($pages = get_pages()) {
                                foreach ($pages as $page) {
                                    $selected = ($page->ID == get_option('ym_success')) ? ' selected' : '';
                                    echo '<option value="'.$page->ID.'"'.$selected.'>'.$page->post_title.'</option>';
                                }
                            }
                            ?></select>
                        <br/><span style="line-height: 1;font-weight: normal;font-style: italic;font-size: 12px;">Эту страницу увидит покупатель, когда оплатит заказ<span>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Страница отказа</th>
                    <td><select id="ym_fail" name="ym_fail">
                            <option value="wc_checkout" <?php echo((get_option('ym_fail') == 'wc_checkout') ? ' selected' : ''); ?>>
                                Страница оформления заказа от WooCommerce
                            </option>
                            <option value="wc_payment" <?php echo((get_option('ym_fail') == 'wc_payment') ? ' selected' : ''); ?>>
                                Страница оплаты заказа от WooCommerce
                            </option>
                            <?php
                            if ($pages = get_pages()) {
                                foreach ($pages as $page) {
                                    $selected = ($page->ID == get_option('ym_fail')) ? ' selected' : '';
                                    echo '<option value="'.$page->ID.'"'.$selected.'>'.$page->post_title.'</option>';
                                }
                            }
                            ?></select>
                        <br/><span style="line-height: 1;font-weight: normal;font-style: italic;font-size: 12px;">Эту страницу увидит покупатель, если что-то пойдет не так: например, если ему не хватит денег на карте<span>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Страница успеха для способа «Оплата картой при доставке»</th>
                    <td><select id="ym_page_mpos" name="ym_page_mpos">
                            <?php
                            if ($pages = get_pages()) {
                                foreach ($pages as $page) {
                                    $selected = ($page->ID == get_option('ym_page_mpos')) ? ' selected' : '';
                                    echo '<option value="'.$page->ID.'"'.$selected.'>'.$page->post_title.'</option>';
                                }
                            }
                            ?></select>
                        <br/>
                        <span style="line-height: 1;font-weight: normal;font-style: italic;font-size: 12px;">Это страница с информацией о доставке. Укажите на ней, когда привезут товар и как его можно будет оплатить<span>
                    </td>
                </tr>
            </table>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Отправлять в Яндекс.Кассу данные для чеков (54-ФЗ)</th>
                    <td>
                        <input type="checkbox" id="ym_enable_receipt"
                               name="ym_enable_receipt" <?php echo get_option('ym_enable_receipt') == 'on' ? "checked" : ""; ?> >
                    </td>
                </tr>
                <tr valign="top">
                    <th></th>
                    <td>
                        <?php if (get_option('ym_enable_receipt')): ?>

                            <?php
                            $wcTaxes        = yamoney_getAllTaxes();
                            $wcCalcTaxes    = get_option('woocommerce_calc_taxes');
                            $ymTaxRatesEnum = get_option('ym_tax_rates_enum');
                            ?>

                            <table class="form-table">
                                <tr valign="top">
                                    <th scope="row">Ставка по умолчанию</th>
                                    <td>
                                        <select id="ym_default_tax_rate" name="ym_default_tax_rate">
                                            <?php foreach ($ymTaxRatesEnum as $taxId => $taxName) : ?>
                                                <option value="<?php echo $taxId ?>" <?php echo $taxId == get_option('ym_default_tax_rate') ? 'selected=\'selected\'' : ''; ?>><?php echo $taxName ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <br/>
                                        <span style="line-height: 1;font-weight: normal;font-style: italic;font-size: 12px;">Ставка по умолчанию будет в чеке, если в карточке товара не указана другая ставка. <span>
                                    </td>
                                </tr>
                            </table>

                            <?php if ($wcCalcTaxes == 'yes' && $wcTaxes) : ?>
                                <table class="form-table">
                                    <tr valign="top">
                                        <th scope="row">Ставка в вашем магазине</th>
                                        <td>
                                            <span style="line-height: 1;font-weight: normal;font-style: italic;font-size: 12px;">Слева - ставка НДС в вашем магазине, справа - в Яндекс.Кассе. Пожалуйста, сопоставьте их.<span>
                                        </td>
                                    </tr>
                                </table>
                                <table class="form-table">
                                    <?php
                                    $ymTaxes = get_option('ym_tax_rate');
                                    ?>
                                    <?php foreach ($wcTaxes as $wcTax) : ?>
                                        <tr valign="top">
                                            <th scope="row" style="width: 50px;"><?php echo round($wcTax->tax_rate) ?>%
                                            </th>
                                            <td style="vertical-align: top; text-align: left;padding: 20px 10px 20px 0;width: 210px;line-height: 1.3;">
                                                передавать в Яндекс.Кассу как
                                            </td>
                                            <td>
                                                <div>
                                                    <?php $selected = isset($ymTaxes[$wcTax->tax_rate_id]) ? $ymTaxes[$wcTax->tax_rate_id] : null; ?>
                                                    <select id="ym_tax_rate[<?php echo $wcTax->tax_rate_id ?>]"
                                                            name="ym_tax_rate[<?php echo $wcTax->tax_rate_id ?>]">
                                                        <?php foreach ($ymTaxRatesEnum as $taxId => $taxName) : ?>
                                                            <option value="<?php echo $taxId ?>" <?php echo $selected == $taxId ? 'selected' : ''; ?> >
                                                                <?php echo $taxName ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <br/>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>


                            <?php endif; ?>
                            <table class="form-table">
                                <tr valign="top">
                                    <th scope="row"><?= __('Предмет расчёта и способ расчёта (ФФД 1.05)',
                                            'yandexcheckout'); ?></th>
                                </tr>
                            </table>

                            <table class="form-table">
                                <tr valign="top">
                                        <span class="help-text">
                                            <?= 'Признаки предмета расчёта и способа расчёта берутся из атрибутов товара paymentSubject и paymentMode. Их значения можно задать отдельно в карточке товара, если это потребуется. <a href="https://kassa.yandex.ru/docs/guides/#perehod-na-ffd-1-05">Подробнее.</a>'; ?>
                                            <span>

                                        <span class="help-text">
                                            <?= 'Для товаров, у которых значения этих атрибутов не заданы, будем применять значения по умолчанию:'; ?>
                                            <span>
                                </tr>
                            </table>

                            <table class="form-table">
                                <tr valign="top">
                                    <th scope="row"><?= 'Предмет расчёта'; ?></th>
                                    <td>
                                        <select id="ym_payment_subject_default"
                                                name="ym_payment_subject_default">
                                            <?php foreach ($paymentSubjectEnum as $id => $subjectName) : ?>
                                                <option value="<?php echo $id ?>" <?php echo $id == get_option(
                                                    'ym_payment_subject_default'
                                                ) ? 'selected=\'selected\'' : ''; ?>><?php echo $subjectName ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <br/>

                                    </td>
                                </tr>
                            </table>

                            <table class="form-table">
                                <tr valign="top">
                                    <th scope="row"><?= 'Способ расчёта'; ?></th>
                                    <td>
                                        <select id="ym_payment_mode_default" name="ym_payment_mode_default">
                                            <?php foreach ($paymentModeEnum as $id => $modeName) : ?>
                                                <option value="<?php echo $id ?>" <?php echo $id == get_option(
                                                    'ym_payment_mode_default'
                                                ) ? 'selected=\'selected\'' : ''; ?>><?php echo $modeName ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <br/>
                                    </td>
                                </tr>
                            </table>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>

            <input type="hidden" name="action" value="update"/>
            <input type="hidden" name="page_options"
                   value="ym_Scid,ym_ShopID,ym_shopPassword,ym_Demo,ym_success,ym_fail,ym_page_mpos,ym_paymode, ym_enable_receipt, ym_default_tax_rate, ym_tax_rate"/>
            <p class="submit">
                <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>"/>
            </p>

        </form>
    </div>
    <?php
}

add_action('parse_request', 'yamoney_checkPayment');

function after_update_setting($one)
{
    new yamoney_statistics();
}

function yamoney_checkPayment()
{
    global $wpdb;
    if (isset($_REQUEST['yandex_money']) && $_REQUEST['yandex_money'] == 'check') {
        $hash = md5($_POST['action'].';'.$_POST['orderSumAmount'].';'.$_POST['orderSumCurrencyPaycash'].';'.
                    $_POST['orderSumBankPaycash'].';'.$_POST['shopId'].';'.$_POST['invoiceId'].';'.
                    $_POST['customerNumber'].';'.get_option('ym_shopPassword'));
        header('Content-Type: application/xml');
        $code        = 1;
        $techMessage = 'bad md5';
        if (isset($_POST['md5']) && strtolower($hash) == strtolower($_POST['md5'])) {
            $order      = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'posts WHERE ID = '.(int)$_POST['customerNumber']);
            $order_summ = (isset($order->ID)) ? get_post_meta($order->ID, '_order_total', true) : 0;
            if ($order) {
                if ($order_summ != $_POST['orderSumAmount']) { // !=
                    $code        = 100;
                    $techMessage = 'wrong orderSumAmount';
                } else {
                    $code        = 0;
                    $techMessage = 'completed';
                    $order_w     = new WC_Order($order->ID);
                    if ($_POST['action'] == 'paymentAviso') {

                        $fields   = array(
                            'b2b_full_name'   => 'Полное наименование',
                            'b2b_short_name'  => 'Сокращенное наименование',
                            'b2b_adress'      => 'Адрес',
                            'b2b_inn'         => 'ИНН',
                            'b2b_kpp'         => 'КПП',
                            'b2b_bank'        => 'Наименование банка',
                            'b2b_bank_office' => 'Отделение банка',
                            'b2b_bik'         => 'БИК',
                            'b2b_account'     => 'Номер счета',
                        );
                        $messages = array();
                        foreach ($fields as $field => $caption) {
                            if (isset($_POST[$field])) {
                                $messages[] = $caption.': '.$_POST[$field];
                            }
                        }
                        $text = empty($messages) ? '' : implode("\n", $messages);

                        $order_w->payment_complete();
                        $order_w->add_order_note("Номер транзакции ".$_POST['invoiceId'].", Сумма оплаты ".$_POST['orderSumAmount'].' '.$text);
                        //$order_w->update_status($techMessage, __( 'Awaiting BACS payment', 'woocommerce' ));
                    }
                }
            } elseif ($_POST['paymentType'] == 'MP') {
                $code        = 0;
                $techMessage = 'Mpos ok';
            } else {
                $code        = 200;
                $techMessage = 'wrong customerNumber';
            }
        }
        $action  = ($_POST['action'] == 'checkOrder') ? "checkOrder" : "paymentAviso";
        $invoice = (intval($_POST['invoiceId']) > 0) ? $_POST['invoiceId'] : 0;
        $answer  = '<?xml version="1.0" encoding="UTF-8"?>
			<'.$action.'Response performedDatetime="'.date('c').'" code="'.$code.'" invoiceId="'.$invoice.'" shopId="'.get_option('ym_ShopID').'" techMessage="'.$techMessage.'"/>';
        @ob_clean();
        die($answer);
    }
}

class yamoney_statistics
{
    public function __construct()
    {
        $this->send();
    }

    private function send()
    {
        global $wp_version;
        $epl         = (bool)(get_option('ym_paymode') == '1');
        $array       = array(
            'url'      => get_option('siteurl'),
            'cms'      => 'wordpress-woo',
            'version'  => $wp_version,
            'ver_mod'  => YAMONEY_VERSION,
            'yacms'    => false,
            'email'    => get_option('admin_email'),
            'shopid'   => get_option('ym_ShopID'),
            'settings' => array(
                'kassa'     => true,
                'kassa_epl' => $epl,
            ),
        );
        $array_crypt = base64_encode(serialize($array));

        $url     = 'https://statcms.yamoney.ru/v2/';
        $curlOpt = array(
            CURLOPT_HEADER         => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLINFO_HEADER_OUT    => true,
            CURLOPT_POST           => true,
        );

        $curlOpt[CURLOPT_HTTPHEADER] = array('Content-Type: application/x-www-form-urlencoded');
        $curlOpt[CURLOPT_POSTFIELDS] = http_build_query(array('data' => $array_crypt, 'lbl' => 0));

        $curl = curl_init($url);
        curl_setopt_array($curl, $curlOpt);
        $rbody = curl_exec($curl);
        $errno = curl_errno($curl);
        $error = curl_error($curl);
        $rcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
    }
}

function yamoney_getAllTaxes()
{
    global $wpdb;

    $query = "
			SELECT *
			FROM {$wpdb->prefix}woocommerce_tax_rates
			WHERE 1 = 1
		";

    $order_by = ' ORDER BY tax_rate_order';

    $result = $wpdb->get_results($query.$order_by);

    return $result;
}
