��    K      t  e   �      `     a  
   h     s     �  
   �  ,   �  i   �  
   1  !   <  
   ^  %   i  !   �     �  #   �     �     �     �       "        B     J     f      l     �     �     �     �     �     �     	     	  -   &	  !   T	     v	     �	     �	     �	  $   �	     �	     
     $
     3
     I
     N
     l
     �
  $   �
     �
     �
     �
     �
     �
     �
                  *   ,     W  	   `  <   j  !   �     �     �  �     Z   �  G     .   c  "   �  '   �     �  5   �       &   -     T  �  s     L     a  $   y     �     �  k   �  �   =       Q   *  #   |  o   �  K        \  `   o  1   �        M   #     q  \   �     �  I   �     D  A   Q  #   �  '   �     �  4   �  G   0  G   x     �  !   �  r   �  ^   i  %   �  G   �  #   6  S   Z     �  5   �  9   �  0   /  9   `     �  /   �  /   �  )        1  	   A     K     f  ,   w     �     �  
   �     �  -   �  h        �     �  �   �  B   6  7   y  F   �  X  �  �   Q  y   �  `   l   S   �   g   !!     �!  g   �!      �!  m   "  A   �"                "   A   #             %                 B   -       5   F   ;   8           ?           3       E   1   *   K   2   
   &          C      :       !          G   7   @   )          =               /                     <         4   $   I                .   >   	         0          D      6                  J      (   +   ,       9      H       '               (Copy) (no title) ALL categories ALL products Applied on Are you sure you want to remove this option? Before adding Extra Product Options, add and save some attributes on the <strong>Attributes</strong> tab. Categories Clear all additional Global forms Clone form Could not update post in the database Delete all options on the product Description Disable Exclude from Global Options Disable categories Disabled Disabled roles for this form Docs Enable Exclude from Global Options Enabled Enabled roles for this form Error Exclude Product(s) from the form Exclude Products Excluded Products Export form Extra Product Options Extra Product Options Fees Failed to write file to disk. Fees amount File imported. File manager is not supported on your server. File upload stopped by extension. Fixed amount Include additional Global forms Invalid data! Invalid import method used. Item name in quotes&ldquo;%s&rdquo; Main Page (no parent) Missing a temporary folder. No file found. No file was uploaded. None Percent of the original price Percent of the orignal price Premium Support Price range: from-to%1$s&ndash;%2$s Price: Priority Products Products and Categories Publish Required Roles SETTINGS Search for a product&hellip; Select the Product(s) to apply the options Settings Settings: Sorry, this file type is not permitted for security reasons. TM Extra Product Option #%s of %s TM Extra Product Options TM Global Extra Product Options The php functions <code>mb_detect_encoding</code> and <code>mb_convert_encoding</code> are required to import and export CSV files. Please ask your hosting provider to enable this function. The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form. The uploaded file exceeds the upload_max_filesize directive in php.ini. The uploaded file was only partially uploaded. There are no saved variations yet. There was an error saving the settings. Title Trying to upload files larger than %s is not allowed! Variation #%s of %s You cannot add any more extra options. Your settings have been saved. Project-Id-Version: TM WooCommerce Extra Product Options
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-12-19 13:33+0200
PO-Revision-Date: 2018-12-19 14:56+0200
Last-Translator: themecomplete <themecomplete@gmail.com>
Language-Team: themecomplete <themecomplete@gmail.com>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
 (Αντιγραφή) (χωρίς τίτλο) ¨Όλες οι κατηγορίες Όλα τα προϊόντα Εφαρμογή σε Είστε βέβαιοι ότι θέλετε να καταργήσετε αυτήν την επιλογή; Πριν την προσθήκη Επιπλέον Επιλογών, προσθέστε και αποθηκεύστε Ιδιότητες στην καρτέλα <strong>Ιδιότητες Προϊόντος</strong>. Κατηγορίες Καταργήστε όλες τις επιπλέον Γενικές Φόρμες Κλωνοποίηση φόρμας Δεν ήταν δυνατή η ενημέρωση της ανάρτησης στη βάση δεδομένων Διαγραφή όλων των επιλογών του προϊόντος Περιγραφή Απενεργοποιήστε την εξαίρεση από τις Γενικές Φόρμες Απενεργοποίηση κατηγοριών Απενεργοποιημένο Απενεργοποιημένοι ρόλοι για αυτή τη φόρμα Έγγραφα Ενεργοποιήστε την εξαίρεση από τις Γενικές Φόρμες Ενεργοποιημένο Ενεργοποιημένοι ρόλοι για αυτή τη φόρμα Σφάλμα Εξαίρεση προϊόντος (ων) από τη φόρμα Εξαίρεση προϊόντων Εξαιρούμενα προϊόντα Εξαγωγή φόρμας Επιπλέον Επιλογές Προϊόντος Κόμιστρα (Επιπλέον Επιλογές Προϊόντος) Αποτυχία εγγραφής του αρχείου σε δίσκο Ποσό φόρου Το αρχείο εισήχθη, Ο διαχειριστής αρχείων δεν υποστηρίζεται στον διακομιστή σας. Η μεταφόρτωση αρχείων σταμάτησε λόγω της επέκτασης Προκαθορισμένο ποσό Συμπεριλάβετε επιπλέον Γενικές Φόρμες Μη έγκυρα δεδομένα! Χρησιμοποιήθηκε μη έγκυρη μέθοδος εισαγωγής. &ldquo;%s&rdquo; Αρχική Σελίδα (χωρίς μητρική) Λείπει ένας προσωρινός φάκελος Δεν βρέθηκε κανένα αρχείο. Δε μεταφορτώθηκε κανένα αρχείο Κανένα Ποσοστό της αρχικής τιμής Ποσοστό της αρχικής τιμής Προνομιακή Υποστήριξη %1$s&ndash;%2$s Τιμή: Προτεραιότητα ΠροΪόντα Προϊόντα και Κατηγορίες Δημοσίευση Απαιτείται Ρόλοι Ρυθμίσεις Αναζήτηση προϊόντος&hellip; Επιλέξτε το/τα προϊόν(ντα) για να εφαρμόσετε τις επιλογές Ρυθμίσεις Ρυθμίσεις Λυπούμαστε, αλλά αυτός ο τύπος αρχείου δεν επιτρέπεται για λόγους ασφαλείας. Επιπλέον Επιλογές Προϊόντος #%s από %s TM Επιπλέον Επιλογές Προϊόντος TM Γενικές Επιπλέον Επιλογές Προϊόντος Οι λειτουργίες php <code>mb_detect_encoding</code> και <code>mb_convert_encoding</code> απαιτούνται για την εισαγωγή και την εξαγωγή αρχείων CSV. Ζητήστε από τον πάροχο φιλοξενίας σας να ενεργοποιήσει αυτή τη λειτουργία. Το αρχείο που μεταφορτώθηκε υπερβαίνει την οδηγία MAX_FILE_SIZE  που καθορίζεται στη φόρμα HTML Το μεταφορτωμένο αρχείο υπερβαίνει την οδηγία upload_max_filesize στο php.ini. Το μεταφορτωμένο αρχείο μετοφορτώθηκε μόνο εν μέρει Δεν υπάρχουν αποθηκευμένες παραλλαγές ακόμα. Παρουσιάστηκε σφάλμα κατά την αποθήκευση των ρυθμίσεων. Τίτλος Δεν επιτρέπεται η μεταφόρτωση αρχείων μεγαλύτερων από%s! Παραλλαγή #%s από %s Δεν μπορείτε να προσθέσετε περισσότερες επιπλέον επιλογές. Οι ρυθμίσεις σας έχουν αποθηκευτεί. 