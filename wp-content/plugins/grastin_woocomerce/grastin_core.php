<?php
/**
 * Plugin Name: Grastin Shipping
 * Description: Grastin Shipping Method for WooCommerce
 * Version: 1.0.0
 * Author: Vadim Anikin
 */
 
if ( ! defined( 'WPINC' ) ) {
    die;
}

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	
////////////	


	function show_grastin_map(){
		global $woocommerce;
	$cart_weight = ceil($woocommerce->cart->cart_contents_weight);
		if($cart_weight >= 25){
				unset( WC()->session->shipping_calculated_cost);
			
			echo '<div class="vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-warning"><div class="vc_message_box-icon"><i class="fa fa-exclamation-triangle"></i></div><p>Масса заказа превышает 25 кг</p></div>';
		}else{
			echo '<div id="gWidget" data-add-cost="10Руб" data-from-city="Москва" data-from-hide="1" data-from-single="1" data-no-weight="1" data-no-partners="post" data-weight-base="'.$cart_weight.'" style="height:500px; margin-bottom:20px;"></div><script src="https://grastin.ru/js/gWidget.js" async></script><script>
			(function (w, d, $) {
				window.grastinPvzWidgetCallback = function (data) {
					switch( true ){
	
	case (data["deliveryType"]== "pvz" && "partnerId" in data && data["partnerId"] !== "" && "cost" in data && data["cost"]!== "" && "pvzData" in data):
		
		jQuery("input[name=shipping_address_1]").val("Доставка в пункт самовывоза " + data["partnerId"] + " по адресу: " + data["pvzData"]["name"]);
		jQuery("input[name=shipping_address_1]").attr("readonly", "readonly");
		
		data.action="add_shipping_cost";
		jQuery.ajax({
			type: "POST",
			url: "'.admin_url( "admin-ajax.php" ).'",
			data: data,
			success: function(){
							jQuery("body").trigger("update_checkout");
							console.log(data);
						}
					});
		
		break;
		
	case (data["deliveryType"]== "courier" && "partnerId" in data && data["partnerId"] !== "" && "cost" in data && data["cost"]!== ""):
		
		jQuery("input[name=shipping_address_1]").val("Доставка курьером " + data["partnerId"] + " по городу: " + data["cityTo"]);
		jQuery("input[name=shipping_address_1]").attr("readonly", "readonly");
		data.action="add_shipping_cost";
		jQuery.ajax({
			type: "POST",
			url: "'.admin_url( "admin-ajax.php" ).'",
			data: data,
			success: function(){
							jQuery("body").trigger("update_checkout");
							console.log(data);
						}
					});
		
		break;
		
		
	default:
	
	jQuery("input[name=shipping_address_1]").val("");					
	jQuery("input[name=shipping_address_1]").removeAttr("readonly");					
	data.cost="0";
	data.action="add_shipping_cost";
		jQuery.ajax({
			type: "POST",
			url: "'.admin_url( "admin-ajax.php" ).'",
			data: data,
			success: function(){
							jQuery("body").trigger("update_checkout");
							console.log(data);
						}
					});
		
};
				};
			})(this, this.document, jQuery);</script>';
		}
	}
	add_action( 'woocommerce_before_checkout_shipping_form', 'show_grastin_map' );
	
		
	function add_shipping_cost(){
			$shipping_cost = $_POST['cost'];
			WC()->session->set( 'shipping_calculated_cost', $shipping_cost );
		wp_die(); 
	}
	
	add_action( 'wp_ajax_add_shipping_cost', 'add_shipping_cost' );
	add_action( 'wp_ajax_nopriv_add_shipping_cost', 'add_shipping_cost');
	
	
	function update_shipping_costs_based_on_cart_session_custom_data( $rates, $package ){
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ){
			return $rates;
		}
		// SET HERE the default cost (when "calculated cost" is not yet defined)
		$cost = '0';

		// Get Shipping rate calculated cost (if it exists)
		$calculated_cost = WC()->session->get( 'shipping_calculated_cost');
		// Iterating though Shipping Methods
			foreach ( $rates as $rate_key => $rate ){
				$method_id = $rate->method_id; $rate_id = $rate->id;
					if( ! empty( $calculated_cost ) ) {
					$cost = $calculated_cost;
				}
				$rates[$rate_id]->cost = number_format($rates[$rate_id]->cost * $cost, 2);
	
		}
		return $rates;
	}
	add_filter('woocommerce_package_rates', 'update_shipping_costs_based_on_cart_session_custom_data', 50, 2);
	
	
	add_action('woocommerce_after_single_product_summary', 'color_map');
	
	function color_map (){
		$my_galary = get_field('choose_color');
		If ($my_galary){
			echo "<div style='margin-bottom: 20px;'><h3>Карта цветов</h3>";
				foreach( $my_galary as $image){
				echo do_shortcode('[vc_single_image image="'.$image["ID"].'" img_size="150x60" add_caption="yes" onclick="link_image" el_class="color_map_img"]');
				}
			echo "</div>";
		}
	}
	
	
	function add_custom_column_name($columns) {
    $columns['columns_array_name'] = 'Бонусный счет';
    return $columns;
}
function show_custom_column_values($value, $column_name, $user_id) {
    if ( 'columns_array_name' == $column_name )
        return get_user_meta( $user_id, '_ywpar_user_total_points', true );
    return $value;
}

add_filter('manage_users_columns', 'add_custom_column_name');
add_action('manage_users_custom_column', 'show_custom_column_values', 10, 3);
	


function show_bonus_func(){
	if ( is_user_logged_in()) {
	echo "<a href='https://bormashop.ru/my-account/bonus/'><div class='bonus_vid'>Баланс бонусного счета:<br><span>".get_user_meta( get_current_user_id(), '_ywpar_user_total_points', true )." руб.</span></div></a>";
	}else{
			echo "<a href='https://bormashop.ru/aktsii/'><div class='bonus_vid'>Получите деньги<br>на бонусный счет</div></a>";
	}
}
add_shortcode( 'show_bonus', 'show_bonus_func');

}
?>
