=== Plugin Name ===
Contributors: dreamsorcerer
Donate link: http://sambull.org/
Tags: pagespeed
Requires at least: 4.6
Tested up to: 5.0
Stable tag: trunk,
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Fixes to allow mod_pagespeed to better optimise your site.

== Description ==

**Note**: If you are not using mod_pagespeed on your server, this plugin will not do anything for you.

This plugin applies some fixes to allow mod_pagespeed to better optimise your site.

Currently the plugin performs these steps:

* Strips id and media attributes from stylesheet tags, allowing pagespeed to combine them properly.

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/pagespeed-fixes` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress.

== Frequently Asked Questions ==

.

== Screenshots ==

.

== Changelog ==

= 1.0.1 =
* Fix possible error if mixing quotes.

= 1.0 =
* Initial release.

== Upgrade Notice ==

.
