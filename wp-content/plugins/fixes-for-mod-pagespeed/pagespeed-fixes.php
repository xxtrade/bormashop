<?php
/*
Plugin Name: Fixes for mod_pagespeed
Plugin URI: http://sambull.org
Description: Fixes to allow mod_pagespeed to better optimise your site.
Version: 1.0.1
Author: Sam Bull
Author URI: http://sambull.org
License: GPLv3+
*/

/** Fix stylesheets by removing id and media attributes, they can be combined. */
function psfixes_fix_stylesheets($html) {
    if (preg_match('/rel=(["\'])stylesheet\1/isS', $html)) {
        $html = preg_replace('/id=(["\']).*?\1/isS', '', $html);
        $html = preg_replace('/media=(["\']).*?\1/isS', '', $html);
    }
    return $html;
}
add_filter('style_loader_tag', 'psfixes_fix_stylesheets');
?>
