<?php

/*
Plugin Name: Поле ипорта ID для 1с
Description: ***
Version: 1.0
Author: Аникин В.
*/

include "rapid-addon.php";

$import_id_for_1c = new RapidAddon('Импорт ID для 1С', 'import_id_for_1c');
$import_id_for_1c->add_field('import-id-field', 'ID для 1С', 'text');
$import_id_for_1c->set_import_function('import_id_for_1c_import');
$import_id_for_1c->run();

function import_id_for_1c_import($post_id, $data, $import_options) {
	global $import_id_for_1c;
	if ($import_id_for_1c->can_update_meta('_wc1c_guid', $import_options)) {
		update_post_meta($post_id, '_wc1c_guid', $data['import-id-field']);
	}
	
}